package modele;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JLabel;

import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

/**
 * Classe qui Contient toutes les donn�es essentielle d'un graphique
 * C'est cet Objet qui sera sauvegard� lors de la sauvegarde d'un graphique
 * Il peut �tre soumis � l'interface Serializable
 * 
 * @author Theo
 *
 */
public class ChartModel implements Serializable{
	/**
	 * Le titre du graphique
	 */
	public String title;
	
	/**
	 * L'axe x objet de la classe SerializableAxis qui contient toutes les donn�es d'un axe
	 */
	public SerializableAxis xAxis;
	/**
	 * L'axe y objet de la classe SerializableAxis qui contient toutes les donn�es d'un axe
	 */
	public SerializableAxis yAxis;
	
	/**
	 * Le nombre de series du graphique
	 */
	public int nbSeries = 0;
	
	/**
	 * Collection qui contient toutes les series du graphe dans une HashMap (leurs nom et les points qui leurs sont associ�s dans une ArrayList)
	 */
	public HashMap<String, ArrayList<ChartPoint>> series = new HashMap<String, ArrayList<ChartPoint>>();
	
	/**
	 * Le type du graphique
	 */
	public int chartType;
	
	/**
	 * Constructeur par defaut de ChartModel
	 */
	public ChartModel(){
		
	
	}
}
