/**
 * 
 */
package modele;

import java.awt.BorderLayout;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import javafx.scene.chart.XYChart;
import vue.ChartPane;
import vue.DataTablePane;
import vue.MainContentPane;

/**
 * Classe qui va gerer les donn�es du tableau de visualisation de donn�es (La JTable � afficher dans le DataTablePanel)
 * 
 * @author Theo
 *
 */
public class DataTableModel extends DefaultTableModel implements TableModelListener {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Le modele du graphique source des donn�es du tableau
	 */
	ChartModel chartModel;
	
	/**
	 * Le panel conteneur du tableau
	 */
	MainContentPane mainContentPane;

	/**
	 * Constructeur par defaut de la classe DataTableModel
	 * 
	 * @param parChartModel Le modele du graphique source des donn�es du tableau
	 * @param parMaincontentPane Le conteneur du tableau
	 */
	public DataTableModel(modele.ChartModel parChartModel,MainContentPane parMaincontentPane){
		
		this.chartModel = parChartModel;
		this.mainContentPane = parMaincontentPane;
		
		int rows = 0,nameNumber = 1;
		String[] seriesName = new String[parChartModel.nbSeries+1];
		seriesName[0] = chartModel.xAxis.label;
		
		for (String serieName : parChartModel.series.keySet()){
			
			seriesName[nameNumber] = serieName;
			nameNumber++;
			
			for (modele.ChartPoint points : parChartModel.series.get(serieName)){
				
				rows++;	
			}
		}
		this.setColumnIdentifiers(seriesName);
		System.out.println(this.getColumnName(1));
		this.setRowCount(rows);
		
		int row = 0,col = 1;
		
		for (String serieName : parChartModel.series.keySet()){
			
			for (modele.ChartPoint points : parChartModel.series.get(serieName)){
				this.setValueAt(points.x, row , 0);
				this.setValueAt(points.y, row, col);
				row++;
			}
			
			col++;
		}
		
	}

	/**
	 * Ne Marche pas (pas la bonne approche)
	 */
	@Override
	public void tableChanged(TableModelEvent tEvent) {
					
		
		
        	for (modele.ChartPoint point : chartModel.series.get(this.getColumnName(tEvent.getColumn()))){
        	
        		if (point.x == (Number)this.getValueAt(tEvent.getFirstRow(), 1)){
        			
        			point.y = (Number)this.getValueAt(tEvent.getFirstRow(), tEvent.getColumn());
        			
        			System.out.println(point.y);
        			
        			this.mainContentPane.remove(this.mainContentPane.getChartPane());
        			this.mainContentPane.invalidate();
        			this.mainContentPane.setChartPane(new ChartPane(ChartPane.IMPORTED_LANCH,this.mainContentPane.getChartPane().chartModel));
        			this.mainContentPane.add(this.mainContentPane.getChartPane(), BorderLayout.CENTER);
        			this.mainContentPane.validate();
        			this.mainContentPane.repaint();
        			
        			break;
        		}
        	
        	}
		}
	
	/**
	 * 
	 */
	@Override
	public boolean isCellEditable(int row, int column){
		
		return true;
	}
}
