package modele;

import java.io.Serializable;

/**
 * Classe qui permet de definir des points par leurs abcisses et leurs ordon�es
 * Il peut �tre soumis � l'interface Serializable
 * 
 * @author Theo
 *
 */
public class ChartPoint implements Serializable {

	/**
	 * Abcisse
	 */
	public Number x;
	
	/**
	 * Ordonn�e
	 */
	public Number y;
	
	/**
	 * Constructeur par default de la classe ChartPoint
	 * 
	 * @param parX La valeur de l'abcisse
	 * @param parY La valeur de l'Ordonn�e
	 */
	public ChartPoint(Number parX, Number parY){
		
		this.x = parX;
		this.y = parY;
	}
}
