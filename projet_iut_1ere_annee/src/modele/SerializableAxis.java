package modele;

import java.io.Serializable;

/**
 * Classe qui contient toute les donn�es d'un axe du graphique
 * Il peut �tre soumis � l'interface Serializable
 * 
 * @author Theo
 *
 */
public class SerializableAxis implements Serializable {

	/**
	 * La valeur de debut de l'axe
	 */
	public Number start;
	
	/**
	 * La valeur de fin de l'axe
	 */
	public Number end;
	
	/**
	 * le saut entre les valeurs � afficher
	 */
	public Number tick;
	
	/**
	 * Le nom de l'axe
	 */
	public String label;
	
	/**
	 * Constructeur par defaut de la classe SerializableAxis
	 * 
	 * @param parStart La valeur de debut de l'axe
	 * @param parEnd La valeur de fin de l'axe
	 * @param parTick le saut entre les valeurs � afficher
	 */
	public SerializableAxis(Number parStart, Number parEnd, Number parTick){
		
		this.start = parStart;
		this.end = parEnd;
		this.tick = parTick;
	}
	
	/**
	 * Constructeur qui cr��e un objet vide de la classe SerializableAxis
	 */
	public SerializableAxis(){
		
	}
}
