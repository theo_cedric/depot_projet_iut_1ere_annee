package vue;

import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * classe qui permet l'affichage de la fenetre d'aide de l'application
 * @author Theo and Cedric
 *
 */
public class HelpWindow extends JFrame implements ActionListener{
	
	/**
	 * Panel contenant le texte de la fenetre d'aide
	 */
	JPanel helpPanel;
	
	/**
	 * Label contenant le texte de la fenetre
	 */
	JLabel textLabel;
	/**
	 * Label contenant le texte de la fenetre
	 */
	JLabel labelNom1;
	/**
	 * Label contenant le texte de la fenetre
	 */
	JLabel labelNom2;
	
	
	/**
	 * Le JScrollPane qui va permettre de naviguer plus facilement dans le panel avec des scrollBar.
	 */
	JScrollPane scrollPane;
	
	/**
	 * bouton qui permet de renvoyer vers la pages Facebook de Cedric Pasquier
	 */
	JButton facebookButtonCedric;
	/**
	 * bouton qui permet de renvoyer vers la pages twitter de Cedric Pasquier
	 */
	JButton twitterButtonCedric;

	/**
	 * bouton qui permet de renvoyer vers la pages Facebook de Th�o Ducatez
	 */
	JButton facebookButtonTheo;
	/**
	 * bouton qui permet de renvoyer vers la pages twitter de Th�o Ducatez
	 */
	JButton twitterButtonTheo;
	
	/**
	 * Contrainte qui va permetre de placer les composants dans le panel
	 */
	GridBagConstraints contrainte;
	
	/**
	 * Constructeur par default de la classe HelpWindow
	 * 
	 * @param parTitre le titre de la fenetre qui est d�fini dans MainWindow quand la classe est appelee
	 */
	public HelpWindow(String parTitre){
		
		//Instanciation et ajout des composants
		super(parTitre);
		setSize (380,500); setLocation (500,200);
		helpPanel = new JPanel();
		
		helpPanel.setLayout(new GridBagLayout());
		
		contrainte = new GridBagConstraints();
		
		contrainte.gridy=0;
		contrainte.gridwidth=7;
		
		textLabel = new JLabel("<html>Bienvenu dans l'aide <br/>"
				+ "Dans cette aide nous allons vous expliquer simplement <br/>"  
				+ "comment marche le logiciel.<br/>"  
				+ "<br/>" 
				+ "Tout d'abord l'onglet nouveau graphe vous permet de  <br/>"  
				+ "rentrer des donn�es pour en construire un avec une <br/>"  
				+ "interface guid�e.<br/>"  
				+ "Vous devez entrer le titre et le type de graphique.<br/>" 
				+ "Suivant le type de graphique, les donn�es demand� ne <br/>" 
				+ "seront pas les m�mes.<br/>" 
				+ "<br/>" 
				+ "Si vous faites un graphe en ligne, d'aire, en bulle ou<br/>" 
				+ "dispers�, il vous sera demand� d�entrer le nom des axes,<br/>" 
				+ " la valeur de d�but et de fin sur l�axe X et Y, l'�chelle <br/>" 
				+ "de l�axe X et Y et pour finir le nombres de s�ries du <br/>" 
				+ "graphique.<br/>" 
				+ "Ensuite, il vous sera demand� d�entrer le nom de vos <br/>" 
				+ "s�ries avec la valeur en X et Y de chaque points, il <br/>" 
				+ "est possible d�ajouter un point en appuyant sur le bouton <br/>" 
				+ "juste en dessous, il est �galement possible de passer � la <br/>" 
				+ "s�ries suivantes en appuyant sur le bouton tout en bas.<br/>" 
				+ "<br/>" 
				+ "Si vous choisissez le diagramme en barres, il vous faudra <br/>" 
				+ "saisir d�abord ce que repr�sente les barres, puis le nom de <br/>" 
				+ "l�axe Y, les valeurs de d�but et de fin sur l�axe Y, l��chelle<br/>" 
				+ " de l�axe Y et pour finir le nombre de groupes.<br/>" 
				+ "Ensuite,  entrer le nom du ou des groupe, la valeur en X et<br/>" 
				+ "en Y da la barre, ajouter des barres avec le bouton d�ajout,<br/>" 
				+ "changer de groupe gr�ce au bouton tout en bas (si il y a <br/>" 
				+ "plusieurs groupe).<br/>" 
				+ "<br/>" 
				+ "Il y a aussi un onglet modification qui vous permet de <br/>"  
				+ "r�ajuster votre graphe.<br/>"  
				+ "<br/>" 
				+ "L'onglet importation vous donne la possibilit� d'impoter un <br/>" 
				+ "graphique d�j� existant.<br/>" 
				+ "<br/>" 
				+ "A la suite de la construction d'un graphe, si vous avez <br/>"  
				+ "trouv� le style qui vous convient, vous pouvez utiliser<br/>"  
				+ "l'onglet sauvegarder, il vous sera demand� de choisir<br/>"  
				+ "un dossier de sauvergarde qui poura �tre cr�e ou <br/>"  
				+ "selectionn�.<br/>"  
				+ "<br/>" 
				+ "Le dernier onglet vous sert � quitter le logiciel.<br/>"  
				+ "<br/>" 
				+ "Des raccourcis on aussi �tait ajout� pour faciliter<br/>"  
				+ "l'utilisation :<br/>"  
				+ "<br/>" 
				+ "Graphe / Nouveau Graphe : Ctrl-N<br/>" 
				+ "Graphe / Modifier ce Grahpique : Ctrl-M<br/>" 
				+ "Graphe / Importer un Graphique existant : Ctrl-I<br/>" 
				+ "Graphe / Sauvegarder le graphique : Ctrl-S<br/>" 
				+ "Changer de type de graphe / Graphique d'aire : Ctrl-R<br/>" 
				+ "Changer de type de graphe / Graphique en barre : Ctrl-B<br/>" 
				+ "Changer de type de graphe / Graphique en bulle : Ctrl-U<br/>" 
				+ "Changer de type de graphe / Graphique en ligne : Ctrl-L<br/>" 
				+ "Changer de type de graphe / Graphique dispers� : Ctrl-C<br/>" 
				+ "Aide : Ctrl+A<br/>"  
				+ "Quitter : Ctrl+Q<br/>"  
				+ "<br/>"  
				+ "Pour plus d'information contactez-nous aux liens ci-dessous.<br/>"  
				+ "<br/>" 
				+ "</html>");
		helpPanel.add(textLabel,contrainte);
		
		contrainte.gridy=1;
		contrainte.gridx=4;
		contrainte.gridwidth=2;
		
		labelNom1 = new JLabel("<html>"
				+ "<br/>"
				+ "Cedric Pasquier : <br/>"
				+ "<br/>"
				+ "</html>");			
		helpPanel.add(labelNom1,contrainte);
		
		contrainte.gridy=2;
		contrainte.gridx=2;
		contrainte.gridwidth=2;
		
		facebookButtonCedric = new JButton("Facebook");
		facebookButtonCedric.addActionListener(this);
		helpPanel.add(facebookButtonCedric,contrainte);
		
		contrainte.gridx=5;
		contrainte.gridwidth=2;
		
		twitterButtonCedric = new JButton("Twitter");
		twitterButtonCedric.addActionListener(this);
		helpPanel.add(twitterButtonCedric,contrainte);
		
		contrainte.gridy=3;
		contrainte.gridx=4;
		contrainte.gridwidth=2;
		
		labelNom2 = new JLabel("<html>"
				+ "<br/>"
				+ "Theo Ducatez :<br/>"
				+ "<br/>"
				+ "</html>");
		helpPanel.add(labelNom2,contrainte);
		
		contrainte.gridy=4;
		contrainte.gridx=2;
		contrainte.gridwidth=2;
		
		facebookButtonTheo = new JButton("Facebook");
		facebookButtonTheo.addActionListener(this);
		helpPanel.add(facebookButtonTheo,contrainte);
		
		contrainte.gridx=5;
		contrainte.gridwidth=2;
		
		twitterButtonTheo = new JButton("Twitter");
		twitterButtonTheo.addActionListener(this);
		helpPanel.add(twitterButtonTheo,contrainte);
		
		
		this.add(helpPanel);
		
		this.scrollPane = new JScrollPane(helpPanel,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.scrollPane.getVerticalScrollBar().setUnitIncrement(20);
		this.add(scrollPane);
		
		
		
		this.setVisible(true);
	}
	
	/**
	 * methode ActionPerformed qui va renvoyer vers des URL suite � l'appuis sur les boutons
	 */
	@Override
	public void actionPerformed(ActionEvent parEvt) {
		if (parEvt.getSource() == facebookButtonCedric){
			try {
			    Desktop.getDesktop().browse(new URL("https://www.facebook.com/cedric.pasquier.79").toURI());
			} catch (Exception e) {
			    e.printStackTrace();
			}
		}
		else if(parEvt.getSource() == twitterButtonCedric){
			try {
			    Desktop.getDesktop().browse(new URL("https://twitter.com/Dedridec").toURI());
			} catch (Exception e) {
			    e.printStackTrace();
			}
		}
		else if(parEvt.getSource() == facebookButtonTheo){
			try {
			    Desktop.getDesktop().browse(new URL("https://www.facebook.com/theo.ducatez").toURI());
			} catch (Exception e) {
			    e.printStackTrace();
			}
		}
		else {
			try {
			    Desktop.getDesktop().browse(new URL("https://twitter.com/XxLuMiNoZz").toURI());
			} catch (Exception e) {
			    e.printStackTrace();
			}
		}
		
	}
 
}
