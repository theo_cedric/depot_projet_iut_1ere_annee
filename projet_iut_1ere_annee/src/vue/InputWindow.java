package vue;

import java.awt.CardLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Fenetre de la classe JFrame qui permet d'initiier la cr�ation d'un nouveau graphique, elle
 * contient tout les panels permettant la cr�ation d'un nouveau graphique
 * 
 * @author Theo
 *
 */
public class InputWindow extends JFrame {

	/**
	 * Le contentPane de la fenetre
	 */
	JPanel mainPanel;
	
	/**
	 * Le panel qui permetra de commencer la cr�ation du graphique
	 */
	ChartInfoInputPanel chartInfoPanel;
	
	/**
	 * le cardLayout du contentPane de la fenetre
	 */
	CardLayout mainCardLayout;
	
	/**
	 * Constructeur par defaut de la classe InputWindow
	 * 
	 * @param parTitre Le titre de la fenetre
	 * @param mainContentPane Le panel qui contient le ChartPane qui contient le graphique et son modele
	 */
	public InputWindow(String parTitre, MainContentPane mainContentPane){
		
		super(parTitre);
		setSize (400,500); setLocation (500,200);
		this.setVisible(true);
		this.setAlwaysOnTop(true);
		
		//Ajout du contentPane
		this.mainPanel = new JPanel();
		this.mainCardLayout = new CardLayout();
		this.mainPanel.setLayout(mainCardLayout);
		this.setContentPane(this.mainPanel);
		
		//Ajout du panel de saisie au contentPane
		this.chartInfoPanel = new ChartInfoInputPanel(mainContentPane,this);
		this.mainPanel.add(this.chartInfoPanel,"chart info panel");
		
		this.setContentPane(this.mainPanel);
		this.setVisible(true);
		
	}
	
}
