package vue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;;

/**
 * 
 * Classe qui permet l'affichage de toute l'application
 * et qui contient la fonction main.
 * 
 * @author Theo and Cedric
 *
 */
public class MainWindow extends JFrame implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Barre de menu de la fen�tre
	 */
	private JMenuBar menuBar;
	
	/**
	 * Menu en cascade pour la visualisation 
	 */
	private JMenu visualisationMenu;
	
	/**
	 * Menu en cascade pour la saisie
	 */
	private JMenu inputMenu;
	
	/**
	 * MenuItem pour la cr�ation d'un nouveau graphique
	 */
	private JMenuItem newChartItem;
	/**
	 * MenuItem pour la modiication d'un graphique
	 */
	private JMenuItem modifyChartItem;
	/**
	 * MenuItem pour l'import d'un graphique existant
	 */
	private JMenuItem importChartItem;
	/**
	 * MenuItem pour la sauvegarde d'un graphique dans un fichier
	 */
	private JMenuItem saveChartItem;
	
	/**
	 * MenuItem pour quitter l'application
	 */
	private JMenuItem quitItem;
	
	/**
	 * MenuItem pour avoir de l'aide et afficher la fenetre d'aide
	 */
	private JMenuItem helpMenu;
	
	/**
	 * Le panel qui va contenir le panel du graphique et le panel du talbeau
	 */
	MainContentPane mainContentPane;
	
	/**
	 * Groupe de bouton pour le choix radio de la visualisation du graphique
	 */
	VisualisationButtonGroup visualisationGroup;
	
	/**
	 * Constructeur par default de la classe FenetrePrincipale 
	 * 
	 * @param parTitre Le titre que l'on veut donner a la fenetre.
	 */
	public MainWindow (String parTitre){
		
		//Instanciation et ajout des composants
		super(parTitre);
		mainContentPane = new MainContentPane();
		this.setContentPane(mainContentPane);
		setDefaultCloseOperation (EXIT_ON_CLOSE);
		setSize (1400,600); setLocation (300,150);
		
		//Ajout des composants de menus
		this.menuBar = new JMenuBar();
		
		//Instanciation Pour le menu Graphe
		this.inputMenu = new JMenu("Graphe ");
		this.inputMenu.setIcon(new ImageIcon("Images"+File.separatorChar+"presentation.png"));
		this.newChartItem = new JMenuItem("Nouveau Graphique",new ImageIcon("Images"+File.separatorChar+"new-file.png"));
		this.newChartItem.setAccelerator(KeyStroke.getKeyStroke('N',java.awt.Event.CTRL_MASK));
		this.newChartItem.addActionListener(this);
		this.modifyChartItem = new JMenuItem("Modifier ce Graphique",new ImageIcon("Images"+File.separatorChar+"modify.png"));
		this.modifyChartItem.setAccelerator(KeyStroke.getKeyStroke('M',java.awt.Event.CTRL_MASK));
		this.importChartItem = new JMenuItem("Importer un Graphique existant",new ImageIcon("Images"+File.separatorChar+"import.png"));
		this.importChartItem.addActionListener(this);
		this.importChartItem.setAccelerator(KeyStroke.getKeyStroke('I',java.awt.Event.CTRL_MASK));
		this.saveChartItem = new JMenuItem("Sauvegarder le graphique",new ImageIcon("Images"+File.separatorChar+"save.png"));
		this.saveChartItem.addActionListener(this);
		this.saveChartItem.setAccelerator(KeyStroke.getKeyStroke('S',java.awt.Event.CTRL_MASK));
		
		//Ajout pour le menu Graphe
		this.inputMenu.add(this.newChartItem);
		this.inputMenu.add(new JSeparator());
		this.inputMenu.add(this.modifyChartItem);
		this.inputMenu.add(new JSeparator());
		this.inputMenu.add(this.importChartItem);
		this.inputMenu.add(new JSeparator());
		this.inputMenu.add(this.saveChartItem);
		this.menuBar.add(inputMenu);
		
		this.menuBar.add(new MenuSeparator());
		
		//Instanciation et ajout pour le menu de visualisation
		this.visualisationMenu = new JMenu("Changer de type de graphe  ");
		this.visualisationMenu.setIcon(new ImageIcon("Images"+File.separatorChar+"eye.png"));
		this.visualisationGroup = new VisualisationButtonGroup();
		this.visualisationMenu.add(this.visualisationGroup.areaButton);
		this.visualisationGroup.areaButton.setAccelerator(KeyStroke.getKeyStroke('R',java.awt.Event.CTRL_MASK));
		this.visualisationGroup.areaButton.addActionListener(this);
		this.visualisationMenu.add(this.visualisationGroup.barButton);
		this.visualisationGroup.barButton.setAccelerator(KeyStroke.getKeyStroke('B',java.awt.Event.CTRL_MASK));
		this.visualisationGroup.barButton.addActionListener(this);
		this.visualisationMenu.add(this.visualisationGroup.bubbleButton);
		this.visualisationGroup.bubbleButton.setAccelerator(KeyStroke.getKeyStroke('U',java.awt.Event.CTRL_MASK));
		this.visualisationGroup.bubbleButton.addActionListener(this);
		this.visualisationMenu.add(this.visualisationGroup.lineButton);
		this.visualisationGroup.lineButton.setAccelerator(KeyStroke.getKeyStroke('L',java.awt.Event.CTRL_MASK));;
		this.visualisationGroup.lineButton.addActionListener(this);
		this.visualisationMenu.add(this.visualisationGroup.scatterButton);
		this.visualisationGroup.scatterButton.setAccelerator(KeyStroke.getKeyStroke('C',java.awt.Event.CTRL_MASK));
		this.visualisationGroup.scatterButton.addActionListener(this);
		
		this.menuBar.add(visualisationMenu);
		
		this.menuBar.add(new MenuSeparator());
		
		//Ajout du menuItem d'aide et pour quitter l'application
		this.helpMenu = new JMenuItem("Aide  ",new ImageIcon("Images"+File.separatorChar+"information.png"));
		this.helpMenu.setMaximumSize(new Dimension(outils.Constantes.HELP_MENU_MAX_WIDTH,(this.helpMenu.getPreferredSize().width)));
		this.helpMenu.addActionListener(this);
		this.helpMenu.setAccelerator(KeyStroke.getKeyStroke('A',java.awt.Event.CTRL_MASK));
		this.menuBar.add(this.helpMenu);
		
		this.menuBar.add(new MenuSeparator());
		
		this.quitItem = new JMenuItem("Quitter",new ImageIcon("Images"+File.separatorChar+"cross-circular-button.png"));
		this.quitItem.setMaximumSize(new Dimension(outils.Constantes.QUIT_ITEM_MAX_WIDTH,this.quitItem.getPreferredSize().width));
		this.quitItem.addActionListener(this);
		this.quitItem.setAccelerator(KeyStroke.getKeyStroke('Q',java.awt.Event.CTRL_MASK));
		this.menuBar.add(quitItem);
		
		this.menuBar.add(new MenuSeparator());
		
		this.setJMenuBar(menuBar);	
		
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	
	/**
	 * Fonction main de l'application.
	 * 
	 * @param args Parametres de precompialtion.
	 */
	public static void main(String[] args){
				
		MainWindow mainWindow = new MainWindow("Chart Visualisation !");	
	}
	
	/**
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent parEvt) {
		
		//Si l'on veut quitter l'application
		if (parEvt.getSource() == this.quitItem){
			
			int confirmation = JOptionPane.showConfirmDialog(this, "Etes vous sur de vouloir quitter l'application ? Verifiez que vous avez sauvegard� votre graphique avant",
					"Quitter ?",JOptionPane.OK_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE);
			
			switch(confirmation){
			case JOptionPane.CLOSED_OPTION:
				break;
			case JOptionPane.CANCEL_OPTION:
				break;
			case JOptionPane.OK_OPTION:
				System.exit(0);
			
			}
				
		}
		//Si l'on veut afficher l'aide
		else if (parEvt.getSource() == this.helpMenu){
			
			HelpWindow helpWindow = new HelpWindow("Help");
			helpWindow.setLocationRelativeTo(this);
			
		}
		//Si l'on veut cr�er un nouveau graphique 
		else if (parEvt.getSource() == this.newChartItem){
			
			InputWindow inputWindow = new InputWindow("Cr�ez votre Graphique", this.mainContentPane);
			inputWindow.setLocationRelativeTo(this);
		}
		//Si l'on veut importer un graphique existant
		else if (parEvt.getSource() == this.importChartItem){
			
			final JFileChooser jfc = new JFileChooser();
			jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			int returnVal = jfc.showOpenDialog(this);
			File file;
			
			file = new File(jfc.getSelectedFile().toString());
			
			modele.ChartModel importedChartModel = (modele.ChartModel)outils.FilesMethod.read(file);
			
			//on actualise le tout
			this.mainContentPane.removeAll();
			this.invalidate();
			this.mainContentPane.getChartPane().chartModel = importedChartModel;
			this.mainContentPane.setChartPane(new ChartPane(ChartPane.IMPORTED_LANCH,importedChartModel));
			this.mainContentPane.add(this.mainContentPane.getChartPane(), BorderLayout.CENTER);
			this.mainContentPane.setTablePane(new DataTablePane(importedChartModel,this.mainContentPane));
			this.mainContentPane.add(this.mainContentPane.getTablePane(),BorderLayout.EAST);
			this.validate();
			this.repaint();
			

		}
		//Si l'on veut sauvegarder un graphique
		else if (parEvt.getSource() == this.saveChartItem){
			
			final JFileChooser jfc = new JFileChooser();
			jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			int returnVal = jfc.showSaveDialog(this);
			File file = null;
			
			if (jfc.getSelectedFile().isDirectory()){
				
				file = new File(jfc.getSelectedFile().toString()+File.separator+this.mainContentPane.getChartPane().chart.getTitle());
			}
			else if (jfc.getSelectedFile().isFile()){
				
				file = new File(jfc.getSelectedFile().toString());
			}
			
			outils.FilesMethod.write(file,this.mainContentPane.getChartPane().chartModel);
			
			JOptionPane.showMessageDialog(this,"Graphique sauvegard� avec succ�s !");
		}
		//Si l'on veut transformer en graphique d'aire
		else if (parEvt.getSource() == this.visualisationGroup.areaButton){
			
			this.mainContentPane.getChartPane().chartModel.chartType = outils.Constantes.AREA_CHART;
			this.mainContentPane.remove(this.mainContentPane.getChartPane());
			this.mainContentPane.invalidate();
			this.mainContentPane.setChartPane(new ChartPane(ChartPane.IMPORTED_LANCH,this.mainContentPane.getChartPane().chartModel));
			this.mainContentPane.add(this.mainContentPane.getChartPane(), BorderLayout.CENTER);
			this.mainContentPane.revalidate();
			this.mainContentPane.repaint();
		}
		//Si l'on veut transformer en graphique en barre
		else if (parEvt.getSource() == this.visualisationGroup.barButton){
			
			this.mainContentPane.getChartPane().chartModel.chartType = outils.Constantes.BAR_CHART;
			this.mainContentPane.remove(this.mainContentPane.getChartPane());
			this.mainContentPane.invalidate();
			this.mainContentPane.setChartPane(new ChartPane(ChartPane.IMPORTED_LANCH,this.mainContentPane.getChartPane().chartModel));
			this.mainContentPane.add(this.mainContentPane.getChartPane(), BorderLayout.CENTER);
			this.mainContentPane.revalidate();
			this.mainContentPane.repaint();

		}
		//Si l'on veut transformer en graphique en bulle
		else if (parEvt.getSource() == this.visualisationGroup.bubbleButton){
			
			this.mainContentPane.getChartPane().chartModel.chartType = outils.Constantes.BUBBLE_CHART;
			this.mainContentPane.remove(this.mainContentPane.getChartPane());
			this.mainContentPane.invalidate();
			this.mainContentPane.setChartPane(new ChartPane(ChartPane.IMPORTED_LANCH,this.mainContentPane.getChartPane().chartModel));
			this.mainContentPane.add(this.mainContentPane.getChartPane(), BorderLayout.CENTER);
			this.mainContentPane.revalidate();
			this.mainContentPane.repaint();

		}
		//Si l'on veut transformer en graphique en ligne
		else if (parEvt.getSource() == this.visualisationGroup.lineButton){
			
			this.mainContentPane.getChartPane().chartModel.chartType = outils.Constantes.LINE_CHART;
			this.mainContentPane.remove(this.mainContentPane.getChartPane());
			this.mainContentPane.invalidate();
			this.mainContentPane.setChartPane(new ChartPane(ChartPane.IMPORTED_LANCH,this.mainContentPane.getChartPane().chartModel));
			this.mainContentPane.add(this.mainContentPane.getChartPane(), BorderLayout.CENTER);
			this.mainContentPane.revalidate();
			this.mainContentPane.repaint();

		}
		//Si l'on veut transformer en graphique dispers�
		else if (parEvt.getSource() == this.visualisationGroup.scatterButton){
			
			this.mainContentPane.getChartPane().chartModel.chartType = outils.Constantes.SCATTER_CHART;
			this.mainContentPane.remove(this.mainContentPane.getChartPane());
			this.mainContentPane.invalidate();
			this.mainContentPane.setChartPane(new ChartPane(ChartPane.IMPORTED_LANCH,this.mainContentPane.getChartPane().chartModel));
			this.mainContentPane.add(this.mainContentPane.getChartPane(), BorderLayout.CENTER);
			this.mainContentPane.revalidate();
			this.mainContentPane.repaint();

		}		
	}
}