package vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Cette classe permet de rentrer les premieres donn�es n�cessaires � la cr�ation d'nun graphique (son nom et son type)
 * 
 * @author Cedric et Th�o
 *
 */
public class ChartInfoInputPanel extends JPanel implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Label pour l'ajout du titre du graphe
	 */
	JLabel titleLabel;
	
	/**
	 * Zone de saisie du titre du graphe
	 */
	JTextField titleInput;
	
	/**
	 * JComboBox pour le choix du type de graphe � cr�er 
	 */
	JComboBox type;
	/**
	 * label pour le choix du type de graphique � cr�er
	 */
	JLabel typeLabel;
	
	/**
	 * Bouton qui permet de passer � la carte suivante qui permet de donner les inforamtions sur les axes du graphe
	 * (differente si c'est un graphique en barre ou non)
	 */
	JButton nextButton;
	
	/**
	 * le panel qui contient le panel qui contient le graphique
	 */
	MainContentPane mainContentPane;
	
	/**
	 * La contrainte s'appliquant sur le panel
	 */
	GridBagConstraints contrainte;

	/**
	 * La fenetre de cr�ation de nouveau graphique qui contient ce panel
	 */
	InputWindow inputWindow;
	
	/**
	 * constructeur par defaut de la classe ChartInfoInputPanel
	 * 
	 * @param mainContentPane Le panel qui contient le ChartPane qui contient le graphique et son modele
	 * @param parInputWindow La fenetre de cr�ation de nouveau graphique qui contient ce panel
	 */
	public ChartInfoInputPanel(MainContentPane mainContentPane,InputWindow parInputWindow){
		
		//initialisation des composants
		this.inputWindow = parInputWindow;
		
		this.mainContentPane = mainContentPane;
		this.setLayout(new GridBagLayout());
		contrainte = new GridBagConstraints();
		
		
		titleLabel = new JLabel("Titre du graphique : ");
		titleInput = new JTextField(8);
		type = new JComboBox(outils.Constantes.CHART_TYPE);
		typeLabel = new JLabel("Type de graphique :");
		
		//ajout des composants
		contrainte.gridy=0;
		contrainte.gridx=0;
		this.add(titleLabel,contrainte);
		contrainte.gridx=4;
		this.add(titleInput,contrainte);
		
		contrainte.gridy = 12;
		contrainte.gridx = 0;
		this.add(typeLabel,contrainte);
		contrainte.gridx = 4;
		this.add(type,contrainte);
		
		contrainte.gridy=13;
		this.add(new JLabel(" "),contrainte);
		contrainte.gridy=14;
		this.add(new JLabel(" "),contrainte);
		
		contrainte.gridy=15;
		contrainte.gridx=1;
		nextButton = new JButton("suite");
		nextButton.addActionListener(this);
		this.add(nextButton,contrainte);	
	}

	/**
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent parEvt) {
		if (parEvt.getSource() == this.nextButton){
		
			modele.ChartModel chart = new modele.ChartModel();
			
			//choix du type de graphique � instancier
			
			//si c'est un graphique en ligne
			if(type.getSelectedItem() == outils.Constantes.CHART_TYPE[1]){
				
				chart.chartType = outils.Constantes.LINE_CHART;
				this.inputWindow.mainPanel.add(new ChartInfoInputPanel2(this.inputWindow,mainContentPane,chart),"chart info input panel 2");
			}
			//si c'est un graphique en aire
			if(type.getSelectedItem() == outils.Constantes.CHART_TYPE[0]){
				
				chart.chartType = outils.Constantes.AREA_CHART;
				this.inputWindow.mainPanel.add(new ChartInfoInputPanel2(this.inputWindow,mainContentPane,chart),"chart info input panel 2");
			}
			//si c'est un graphique en barre
			if(type.getSelectedItem() == outils.Constantes.CHART_TYPE[2]){
	
				chart.chartType = outils.Constantes.BAR_CHART;
				this.inputWindow.mainPanel.add(new BarChartInfoInputPanel2(this.inputWindow,mainContentPane,chart),"Bar chart info input panel 2");
			}
			//si c'est un graphique en bulle
			if(type.getSelectedItem() == outils.Constantes.CHART_TYPE[3]){
	
				chart.chartType = outils.Constantes.BUBBLE_CHART;
				this.inputWindow.mainPanel.add(new ChartInfoInputPanel2(this.inputWindow,mainContentPane,chart),"chart info input panel 2");
			}
			//si c'est un graphique dispers�
			if(type.getSelectedItem() == outils.Constantes.CHART_TYPE[4]){
	
				chart.chartType = outils.Constantes.SCATTER_CHART;
				this.inputWindow.mainPanel.add(new ChartInfoInputPanel2(this.inputWindow,mainContentPane,chart),"chart info input panel 2");
			}
			
			chart.title = this.titleInput.getText();
			this.inputWindow.mainCardLayout.next(this.inputWindow.mainPanel);
		}
	}
		
}

