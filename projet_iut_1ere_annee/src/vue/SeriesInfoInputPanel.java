package vue;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import javafx.scene.chart.XYChart;
import modele.ChartPoint;

/**
 * Panel pour la saisie des informations des series ou groupes d'un graphique (different en fonction du type de graphique)
 * 
 * @author Theo
 *
 */
public class SeriesInfoInputPanel extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * label de la s�rie ou du groupe en cr�ation
	 */
	JLabel serie;
	
	/**
	 * Label pour le choix du nom de la serie ou du groupe
	 */
	JLabel serieName;
	/**
	 * zone de saisie pour le nom de la serie ou du groupe
	 */
	JTextField serieNameInput;
	
	/**
	 * label pour les coordon�es x d'un point
	 */
	JLabel xLabel;
	/**
	 * zone de saisie pour les coordon�es x d'un point
	 */
	JTextField xInput;
	/**
	 * zone de saisie pour les coordon�es y d'un point
	 */
	JTextField yInput;
	/**
	 * label pour les coordon�es y d'un point
	 */
	JLabel yLabel;
	
	/**
	 * LayoutManager de ce panel du type GridBagLayout
	 */
	GridBagLayout gridBagLayout;
	/**
	 * Contrainte permettant de placer les composants
	 */
	GridBagConstraints constraint;
	
	/**
	 * Label banal d'indication
	 */
	JLabel labelInfo1;
	/**
	 * label banal d'indication
	 */
	JLabel labelInfo2;
	
	/**
	 * Bouton permetant l'ajout d'un point � la serie ou au groupe
	 */
	JButton addButton;
	/**
	 * Bouton permettant de passer a la serie ou groupe suivant (ou de finaliser � la fin)
	 */
	JButton nextButton;
	
	/**
	 * Le panel qui contient le ChartPane qui contient le graphique et son modele
	 */
	MainContentPane mainContentPane;
	/**
	 * La fenetre qui permet la cr�ation d'un nouveau graphique et qui contient ce panel
	 */
	InputWindow inputWindow;
	/**
	 * Le modele du graphique en trains d'�tre cr��e
	 */
	modele.ChartModel chartModel;
	/**
	 * L'indice de la s�rie en cour de cr�ation
	 */
	int indiceSerie;
	
	/**
	 * liste qui contient les points cr�es qui vont �tre ajout� � la serie ou au groupe
	 */
	ArrayList<ChartPoint> pointList = new ArrayList<ChartPoint>();
	
	/**
	 * Constructeur par defaut de la classe SeriesInfoInputPanel
	 * 
	 * @param mainContentPane Le panel qui contient le ChartPane qui contient le graphique et son modele
	 * @param parInputWindow La fenetre qui permet la cr�ation d'un nouveau graphique et qui contient ce panel
	 * @param parChartModel Le modele du graphique en trains d'�tre cr��e
	 * @param i L'indice de la s�rie en cour de cr�ation
	 */
	public SeriesInfoInputPanel(MainContentPane mainContentPane,InputWindow parInputWindow,modele.ChartModel parChartModel, int i){
		
		//Instanciation des composants
		this.mainContentPane = mainContentPane;
		this.inputWindow = parInputWindow;
		this.chartModel = parChartModel;
		this.indiceSerie = i;
		
		this.gridBagLayout = new GridBagLayout();
		this.setLayout(gridBagLayout);
		this.constraint = new GridBagConstraints();
		
		//Si c'est un graphique en barre on � une approche differente avec des groupes
		if(this.chartModel.chartType == outils.Constantes.BAR_CHART){
			
			this.serie = new JLabel("Groupe num�ro "+Integer.toString(i));
			this.serieName = new JLabel("Nom du groupe : ");
			this.labelInfo1 = new JLabel("Appuyer sur ajouter point pour ajouter un point");
			this.xLabel = new JLabel("Valeur de x (de la barre) : ");
			this.labelInfo2 = new JLabel("Appuyez sur groupe suivant pour changer de groupe");
		}
		//Sinon on reste utilise des series
		else{
			
			this.serie = new JLabel("Serie num�ro "+Integer.toString(i));
			this.serieName = new JLabel("Nom de la serie : ");
			this.labelInfo1 = new JLabel("Appuyer sur ajouter point pour ajouter un point");
			this.xLabel = new JLabel("Valeur de X : ");
			this.labelInfo2 = new JLabel("Appuyez sur serie suivante pour changer de serie");
		}
		
		//Ajout des composants
		constraint.gridx = 0; constraint.gridy = 0;
		this.add(this.serie,constraint);
		
		constraint.gridx = 0; constraint.gridy = 1;
		this.add(serieName, constraint);
		constraint.gridx = 1;constraint.gridy = 1;
		this.serieNameInput = new JTextField(4);
		this.add(this.serieNameInput, constraint);
		
		constraint.gridx = 0; constraint.gridy = 3; constraint.gridwidth = 2;
		
		this.add(labelInfo1, constraint);
		constraint.gridx = 0; constraint.gridy = 4;constraint.gridwidth = 1;
		this.add(xLabel, constraint);
		constraint.gridx = 1;constraint.gridy = 4;
		this.xInput = new JTextField(4);
		this.add(this.xInput, constraint);
		
		constraint.gridx = 0; constraint.gridy = 5;
		this.yLabel = new JLabel("Valeur de Y : ");
		this.add(yLabel, constraint);
		constraint.gridx = 1;constraint.gridy = 5;
		this.yInput = new JTextField(4);
		this.add(this.yInput, constraint);
		
		constraint.gridx = 0; constraint.gridy = 6;
		this.addButton = new JButton("Ajouter point !");
		this.addButton.addActionListener(this);
		this.add(addButton, constraint);
		
		constraint.gridx = 0; constraint.gridy = 7;constraint.gridwidth = 2;
		this.add(labelInfo2, constraint);
		
		constraint.gridx = 0; constraint.gridy = 8;constraint.gridwidth = 1;
		
		//Si c'est la derniere serie ou groupe on remplace le texte du bouton par finaliser !
		if (this.indiceSerie != this.chartModel.nbSeries){
		
			//Si c'est un graphique en barre on a des groupes sinon des series
			if(this.chartModel.chartType != outils.Constantes.BAR_CHART){
				this.nextButton = new JButton("Serie Suivante");
			}
			else{
				this.nextButton = new JButton("Groupe suivant");
			}
		}
		else{
			
			this.nextButton = new JButton("Finaliser !");
		}
		this.nextButton.addActionListener(this);
		this.add(nextButton, constraint);
	}

	/**
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	
		//Si on passe � la serie suivante
		if (e.getSource() == this.nextButton){
			
			//si ce n'est pas la derniere serie
			if (this.indiceSerie != this.chartModel.nbSeries){
			
				this.chartModel.series.put(this.serieNameInput.getText(), this.pointList);
				this.inputWindow.mainCardLayout.next(this.inputWindow.mainPanel);
			}
			//si c'est la derniere serie
			else{
				
				this.chartModel.series.put(this.serieNameInput.getText(), this.pointList);
				this.inputWindow.dispatchEvent(new WindowEvent(this.inputWindow, WindowEvent.WINDOW_CLOSING));
				
				Number maxValue = 0;
				Number minValue = 1000000000;
				
				//Si c'est un graphique en barre on regarde pour pouvoir mettre un axe des abcisses pour les autres visualisations correcte
				//regarder ses valeurs max et minimum
				if(chartModel.chartType == outils.Constantes.BAR_CHART){
					
					for (String i : chartModel.series.keySet()){
			
			        	for (modele.ChartPoint point : chartModel.series.get(i)){
					
							if(point.x.doubleValue() > maxValue.doubleValue()){
								
								maxValue = point.x;
							}
							if(point.x.doubleValue() < minValue.doubleValue()){
								
								minValue = point.x;
							}
			        	}
					}
					double minus = -1;
					String name = chartModel.xAxis.label.toString();
					chartModel.xAxis = new modele.SerializableAxis((minValue.doubleValue()+minus),maxValue.doubleValue()+1,0);
					chartModel.xAxis.label = name;
				}
				//On actualise le tout ! pour afficher le nouveau graphique !
				this.mainContentPane.removeAll();
				this.mainContentPane.invalidate();
				this.mainContentPane.getChartPane().chartModel = this.chartModel;
				this.mainContentPane.setChartPane(new ChartPane(ChartPane.IMPORTED_LANCH,this.chartModel));
				this.mainContentPane.add(this.mainContentPane.getChartPane(), BorderLayout.CENTER);
				this.mainContentPane.setTablePane(new DataTablePane(this.chartModel,this.mainContentPane));
				this.mainContentPane.add(this.mainContentPane.getTablePane(),BorderLayout.EAST);
				this.mainContentPane.validate();
				this.mainContentPane.repaint();
			}
		}
		//Quand on ajoute un point a une serie ou un groupeserie
		else if (e.getSource() == this.addButton){
			
			this.pointList.add(new modele.ChartPoint(Float.parseFloat(this.xInput.getText()),Float.parseFloat(this.yInput.getText())));
			this.xInput.setText("");
			this.yInput.setText("");
		}
	}
}
