package vue;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.BubbleChart;
import javafx.scene.chart.Chart;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.CategoryAxis;
/**
 * Classe qui contient le jfxPanel qui contient le graphique g�n�r� en javaFX et qui l'initialise � partir de son modele
 * 
 * @author Theo
 *
 */
public class ChartPane extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Le graphique a afficher
	 */
	Chart chart;
	
	/**
	 * Le modele du graphique
	 */
	public modele.ChartModel chartModel;
	
	/**
	 * Int qui reference la cr�ation du panel lors du premier lancement de l'application
	 */
	public static final int FIRST_LANCH = 1;
	
	/**
	 * Int qui reference la cr�ation du panel lors des importations ou des cr�ations de graphe (autres cas que first lanch)
	 */
	public static final int IMPORTED_LANCH = 2;
	
	/**
	 * Le panel JFXPanel qui va contenir le graphe
	 */
	final JFXPanel jfxChartPanel = new JFXPanel();
	
	/**
	 * Constructeur par defaut de la classe ChartPane
	 * 
	 * @param i Definit si c'est un lancement import� ou si c'est le premier lancement de l'application (avec les constantes de la classe CharPane)
	 * @param parChartModel Le modele du graphique � afficher
	 */
	public ChartPane(int i,modele.ChartModel parChartModel){	
		
			this.setBackground(new Color(245, 204, 163));
			this.chartModel = parChartModel;
			this.add(jfxChartPanel);
			Platform.runLater(new Runnable() {
		
				@Override
				public void run() {
			
					initFX(jfxChartPanel, i);
				}
			});
			Platform.setImplicitExit(false);
	}
	
	/**
	 * Methode qui permet d'initialiser le panel javaFX avec le graphe
	 * 
	 * @param jfxPanel Le panel JFX qui doit �tre initialis� et qui devra contenir le graphe
	 * @param i Definit si c'est un lancement import� ou si c'est le premier lancement de l'application (avec les constantes de la classe CharPane)
	 */
	public void initFX(JFXPanel jfxPanel,int i) {
		
		Group root = new Group();
		chart = initChart(this.chartModel,i);
		chart.setLayoutX(80);
		root.getChildren().add(chart);
		Scene scene = new Scene(root, 700, 500);
		jfxPanel.setScene(scene);
		
		}
	
	/**
	 * Methode qui permet d'initialiser le graphique grace au modele
	 * 
	 * @param chartModel Le modele � partir du quel on va cr�er le graphique
	 * @param j Definit si c'est un lancement import� ou si c'est le premier lancement de l'application (avec les constantes de la classe CharPane)
	 * 
	 * @return Le graphique initialis� � afficher
	 */
	public static Chart initChart (modele.ChartModel chartModel, int j) {
		
		//si c'est le premier lancement
		if (j == ChartPane.FIRST_LANCH){
			
			chartModel = initModel(chartModel);
		}
		
		//sinon on commence � instancier les axes du graphique
		NumberAxis xAxis = new NumberAxis(chartModel.xAxis.start.doubleValue(),chartModel.xAxis.end.doubleValue(),chartModel.xAxis.tick.doubleValue());
	 	NumberAxis yAxis = new NumberAxis(chartModel.yAxis.start.doubleValue(),chartModel.yAxis.end.doubleValue(),chartModel.yAxis.tick.doubleValue());
	 	xAxis.setLabel(chartModel.xAxis.label);
	 	yAxis.setLabel(chartModel.yAxis.label);
	 	
	 	//si c'est un graphique en ligne
	 	if (chartModel.chartType == outils.Constantes.LINE_CHART){
	 		
	 		//on instancie le graphique
	 		final LineChart<Number,Number> chart = new LineChart<Number,Number>(xAxis,yAxis);
	 		XYChart.Series[] serie = new XYChart.Series[chartModel.nbSeries];
	 		
	 		//On parcour les tableaux de points pour les informations sur les series et leurs points
	 		for (int i = 0; i < chartModel.nbSeries;i++){
				
				serie[i] = new XYChart.Series();
				
				String serieName = (String)chartModel.series.keySet().toArray()[i];
				serie[i].setName(serieName);
	        	for (modele.ChartPoint point : chartModel.series.get(serie[i].getName())){
	        	
	        		serie[i].getData().add(new XYChart.Data(point.x,point.y));
	        	
	        	}   
			}
	 		//On ajoute les series au graphique
	 		chart.getData().addAll(serie);
	        chart.setAnimated(true);
	        chart.setTitle(chartModel.title);
			
	        //on retourne le chart
			return chart;
	 	}
	 	//Si c'est un graphique en barre
	 	else if (chartModel.chartType == outils.Constantes.BAR_CHART){
	 		
	 		//On instancie les axes
	 		CategoryAxis xBarAxis = new CategoryAxis();
	 		xBarAxis.setLabel(chartModel.xAxis.label);
	 		
	 		final BarChart<String,Number> chart = new BarChart<String,Number>(xBarAxis,yAxis);
	 		int axisIntValue = 0;
	 		

	 		//On parcoure pour pouvoir indiquer la valeur max des abcisse des points
	 		for (ArrayList<modele.ChartPoint> value : chartModel.series.values()){
	 				
	 			axisIntValue =0;
	 			
	 			for (modele.ChartPoint point : value){
	        		
	 				axisIntValue++;
	 			}
	 			
	        }
	        	
	 		//On parcour les series pour y prendre les points sauf qu'on applique cela � un graphique en barre qui est une 
	 		//aproche differente des autres graphiques les series sont des groupes et les abcisses sont des series
	 		int compteur = 0;
	 		
	 		XYChart.Series[] serie = new XYChart.Series[axisIntValue];
	 		
	 		for (int i = 0; i < axisIntValue;i++){
				
				serie[i] = new XYChart.Series();
				
				for (int v = 0; v < chartModel.nbSeries;v++){
					
					for (modele.ChartPoint point : chartModel.series.get((String)chartModel.series.keySet().toArray()[v])){
	        	
						if(compteur != i){
							
							compteur++;
						}
						else{
							serie[i].getData().add(new XYChart.Data((String)chartModel.series.keySet().toArray()[v],point.y));
							serie[i].setName(Integer.toString(point.x.intValue()));
							compteur++;
						}
					}
					compteur = 0;
	        	}   
			}
	 		chart.getData().addAll(serie);
	        chart.setAnimated(true);
	        chart.setTitle(chartModel.title);
			
			return chart;
	 	}
	 	//A partir d'ici la description est la meme que le Line Chart on instancie juste un graphique d'un autre type
	 	else if (chartModel.chartType == outils.Constantes.AREA_CHART){
	 		
	 		final AreaChart<Number,Number> chart = new AreaChart<Number,Number>(xAxis,yAxis);
	 		XYChart.Series[] serie = new XYChart.Series[chartModel.nbSeries];
	 		
	 		for (int i = 0; i < chartModel.nbSeries;i++){
				
				serie[i] = new XYChart.Series();
				
				String serieName = (String)chartModel.series.keySet().toArray()[i];
				serie[i].setName(serieName);
	        	for (modele.ChartPoint point : chartModel.series.get(serie[i].getName())){
	        	
	        		serie[i].getData().add(new XYChart.Data(point.x,point.y));
	        	
	        	}   
			}
	 		chart.getData().addAll(serie);
	        chart.setAnimated(true);
	        chart.setTitle(chartModel.title);
			
			return chart;
	 	}
	 	else if (chartModel.chartType == outils.Constantes.BUBBLE_CHART){
	 		
	 		final BubbleChart<Number,Number> chart = new BubbleChart<Number,Number>(xAxis,yAxis);
	 		XYChart.Series[] serie = new XYChart.Series[chartModel.nbSeries];
	 		
	 		for (int i = 0; i < chartModel.nbSeries;i++){
				
				serie[i] = new XYChart.Series();
				
				String serieName = (String)chartModel.series.keySet().toArray()[i];
				serie[i].setName(serieName);
	        	for (modele.ChartPoint point : chartModel.series.get(serie[i].getName())){
	        	
	        		serie[i].getData().add(new XYChart.Data(point.x,point.y));
	        	
	        	}   
			}
	 		chart.getData().addAll(serie);
	        chart.setAnimated(true);
	        chart.setTitle(chartModel.title);
			
			return chart;
	 	}
	 	else if (chartModel.chartType == outils.Constantes.SCATTER_CHART){
	 		
	 		final ScatterChart<Number,Number> chart = new ScatterChart<Number,Number>(xAxis,yAxis);
	 		XYChart.Series[] serie = new XYChart.Series[chartModel.nbSeries];
	 		
	 		for (int i = 0; i < chartModel.nbSeries;i++){
				
				serie[i] = new XYChart.Series();
				
				String serieName = (String)chartModel.series.keySet().toArray()[i];
				serie[i].setName(serieName);
	        	for (modele.ChartPoint point : chartModel.series.get(serie[i].getName())){
	        	
	        		serie[i].getData().add(new XYChart.Data(point.x,point.y));
	        	
	        	}   
			}
	 		chart.getData().addAll(serie);
	        chart.setAnimated(true);
	        chart.setTitle(chartModel.title);
			
			return chart;
	 	}
	 	
	 	return null;
	}
	
	/**
	 * Initie le modele du premier graphique � afficher lors du premier lancement de l'application
	 * 
	 * @param chartModel le tout nouveau modele � partir du quel on doit cr�er le graphique
	 *
	 * @return Le modele du premier graphique � afficher lors du premier lancement de l'application
	 */
	public static modele.ChartModel initModel(modele.ChartModel chartModel){
		
		/*
		chartModel.xAxis = new modele.SerializableAxis(1,12,1);
    	chartModel.yAxis = new modele.SerializableAxis(5,50,1);
    	chartModel.xAxis.label = "Mois";
    	chartModel.yAxis.label = "Nombre d'ordinateurs";
    
    	chartModel.title ="Graphique de d�monstration";
    	
    	chartModel.nbSeries = 1;
    	//defining a series
    	chartModel.series.put("Entreprise 1",new ArrayList<modele.ChartPoint>());
    	//populating the series with data
    	chartModel.series.get("Entreprise 1").add(new modele.ChartPoint(1, 23));
    	chartModel.series.get("Entreprise 1").add(new modele.ChartPoint(2, 14));
    	chartModel.series.get("Entreprise 1").add(new modele.ChartPoint(3, 15));
    	chartModel.series.get("Entreprise 1").add(new modele.ChartPoint(4, 24));
    	chartModel.series.get("Entreprise 1").add(new modele.ChartPoint(5, 34));
    	chartModel.series.get("Entreprise 1").add(new modele.ChartPoint(6, 36));
    	chartModel.series.get("Entreprise 1").add(new modele.ChartPoint(7, 22));
    	chartModel.series.get("Entreprise 1").add(new modele.ChartPoint(8, 45));
    	chartModel.series.get("Entreprise 1").add(new modele.ChartPoint(9, 43));
    	chartModel.series.get("Entreprise 1").add(new modele.ChartPoint(10, 17));
    	chartModel.series.get("Entreprise 1").add(new modele.ChartPoint(11, 29));
    	chartModel.series.get("Entreprise 1").add(new modele.ChartPoint(12, 25));
    
    	chartModel.chartType = outils.Constantes.AREA_CHART;
    	
    	chartModel.nbSeries = 1;*/	
    	
		//initialisation basique d'un graphique en javaFx
		chartModel.xAxis = new modele.SerializableAxis(2004,2017,1);
    	chartModel.yAxis = new modele.SerializableAxis(0,70,5);
    	chartModel.xAxis.label = "Ann�es";
    	chartModel.yAxis.label = "Nombre en millions";
    
    	chartModel.title ="Population Fran�aise depuis 2004";
    	
    	chartModel.series.put("Femmes",new ArrayList<modele.ChartPoint>());

    	chartModel.series.get("Femmes").add(new modele.ChartPoint(2004, 32.1));
    	chartModel.series.get("Femmes").add(new modele.ChartPoint(2005, 32.37));
    	chartModel.series.get("Femmes").add(new modele.ChartPoint(2006, 32.62));
    	chartModel.series.get("Femmes").add(new modele.ChartPoint(2007, 32.82));
    	chartModel.series.get("Femmes").add(new modele.ChartPoint(2008, 33));
    	chartModel.series.get("Femmes").add(new modele.ChartPoint(2009, 33.18));
    	chartModel.series.get("Femmes").add(new modele.ChartPoint(2010, 33.33));
    	chartModel.series.get("Femmes").add(new modele.ChartPoint(2011, 33.49));
    	chartModel.series.get("Femmes").add(new modele.ChartPoint(2012, 33.65));
    	chartModel.series.get("Femmes").add(new modele.ChartPoint(2013, 33.81));
    	chartModel.series.get("Femmes").add(new modele.ChartPoint(2014, 34.1));
    	chartModel.series.get("Femmes").add(new modele.ChartPoint(2015, 34.27));
    	chartModel.series.get("Femmes").add(new modele.ChartPoint(2016, 34.4));
    	chartModel.series.get("Femmes").add(new modele.ChartPoint(2017, 34.54));
    	
    	chartModel.series.put("Hommes",new ArrayList<modele.ChartPoint>());

    	chartModel.series.get("Hommes").add(new modele.ChartPoint(2004, 30.1));
    	chartModel.series.get("Hommes").add(new modele.ChartPoint(2005, 30.33));
    	chartModel.series.get("Hommes").add(new modele.ChartPoint(2006, 30.48));
    	chartModel.series.get("Hommes").add(new modele.ChartPoint(2007, 30.78));
    	chartModel.series.get("Hommes").add(new modele.ChartPoint(2008, 30.9));
    	chartModel.series.get("Hommes").add(new modele.ChartPoint(2009, 31.12));
    	chartModel.series.get("Hommes").add(new modele.ChartPoint(2010, 31.27));
    	chartModel.series.get("Hommes").add(new modele.ChartPoint(2011, 31.41));
    	chartModel.series.get("Hommes").add(new modele.ChartPoint(2012, 31.65));
    	chartModel.series.get("Hommes").add(new modele.ChartPoint(2013, 31.69));
    	chartModel.series.get("Hommes").add(new modele.ChartPoint(2014, 31.8));
    	chartModel.series.get("Hommes").add(new modele.ChartPoint(2015, 32.13));
    	chartModel.series.get("Hommes").add(new modele.ChartPoint(2016, 32.3));
    	chartModel.series.get("Hommes").add(new modele.ChartPoint(2017, 32.36));
    	
    	chartModel.series.put("Totale",new ArrayList<modele.ChartPoint>());

    	chartModel.series.get("Totale").add(new modele.ChartPoint(2004, 62.2));
    	chartModel.series.get("Totale").add(new modele.ChartPoint(2005, 62.7));
    	chartModel.series.get("Totale").add(new modele.ChartPoint(2006, 63.1));
    	chartModel.series.get("Totale").add(new modele.ChartPoint(2007, 63.6));
    	chartModel.series.get("Totale").add(new modele.ChartPoint(2008, 63.9));
    	chartModel.series.get("Totale").add(new modele.ChartPoint(2009, 64.3));
    	chartModel.series.get("Totale").add(new modele.ChartPoint(2010, 64.6));
    	chartModel.series.get("Totale").add(new modele.ChartPoint(2011, 64.9));
    	chartModel.series.get("Totale").add(new modele.ChartPoint(2012, 65.2));
    	chartModel.series.get("Totale").add(new modele.ChartPoint(2013, 65.5));
    	chartModel.series.get("Totale").add(new modele.ChartPoint(2014, 65.9));
    	chartModel.series.get("Totale").add(new modele.ChartPoint(2015, 66.4));
    	chartModel.series.get("Totale").add(new modele.ChartPoint(2016, 66.7));
    	chartModel.series.get("Totale").add(new modele.ChartPoint(2017, 66.9));
    
    	chartModel.chartType = outils.Constantes.LINE_CHART;
    	
    	chartModel.nbSeries = 3;
    	
    	return chartModel;
	}
}