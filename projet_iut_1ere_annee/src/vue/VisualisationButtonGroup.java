package vue;

import java.io.File;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JRadioButtonMenuItem;

/**
 * Classe de ButtonGroup (groupe de bouton) pour le choix radio de visualisation du graphique
 * 
 * @author Theo
 *
 */
public class VisualisationButtonGroup extends ButtonGroup {

	/**
	 * Radio bouton menu item pour le choix de visualisation d'un graphique en ligne
	 */
	JRadioButtonMenuItem lineButton;
	/**
	 * Radio bouton menu item pour le choix de visualisation d'un graphique en aire
	 */
	JRadioButtonMenuItem areaButton;
	/**
	 * Radio bouton menu item pour le choix de visualisation d'un graphique en bulle
	 */
	JRadioButtonMenuItem bubbleButton;
	/**
	 * Radio bouton menu item pour le choix de visualisation d'un graphique dispers� (que des points)
	 */
	JRadioButtonMenuItem scatterButton;
	/**
	 * Radio bouton menu item pour le choix de visualisation d'un graphique en barre
	 */
	JRadioButtonMenuItem barButton;

	/**
	 * Constructeur par defaut de la classe VisualisationButtonGroup
	 */
	public VisualisationButtonGroup() {
		
		super();
		//Instanciation
		this.lineButton = new JRadioButtonMenuItem("Graphique en ligne",new ImageIcon("Images"+File.separatorChar+"line-graph.png"));
		this.areaButton = new JRadioButtonMenuItem("Graphique d'aire",new ImageIcon("Images"+File.separatorChar+"area-chart.png"));
		this.bubbleButton = new JRadioButtonMenuItem("Graphique en bulle",new ImageIcon("Images"+File.separatorChar+"diagram.png"));
		this.scatterButton = new JRadioButtonMenuItem("Graphique dispers�",new ImageIcon("Images"+File.separatorChar+"more.png"));
		this.barButton = new JRadioButtonMenuItem("Graphique en barre",new ImageIcon("Images"+File.separatorChar+"bars-chart.png"));
		//Ajout
		
		
		this.add(lineButton);
		this.add(areaButton);
		this.add(bubbleButton);
		this.add(scatterButton);
		this.add(barButton);
	}

}
