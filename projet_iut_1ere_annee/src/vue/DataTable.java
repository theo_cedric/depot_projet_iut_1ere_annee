/**
 * 
 */
package vue;

import javax.swing.JTable;

/**
 * Classe qui defini un tableau de donn�es
 * 
 * @author Theo
 *
 */
public class DataTable extends JTable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructeur par defaut de la classe DataTable
	 */
	public DataTable(){
		
		this.getTableHeader().setResizingAllowed(false);
		this.setRowHeight(30);
		this.setAutoResizeMode(AUTO_RESIZE_OFF);
		this.getTableHeader().setResizingAllowed(false);
	}
}
