package vue;

import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * Classe d'un panel qui permet de contenir le tableau de donn�es.
 * 
 * @author Theo
 *
 */
public class DataTablePane extends JPanel {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Tableau qui permet de visualiser les donn�es. On l'ajoute
	 * au panel
	 */
	private DataTable dataTable;
	
	/**
	 * Le modele du graphique � partir du quel on doit cr�er le tableau
	 */
	public modele.DataTableModel dataTableModel;
	
	/**
	 * Constructeur par defaut de la classe DataTablePane
	 * 
	 * @param parChartModel le modele du graphique � partir du quel on doit cr�er le tableau
	 * @param parMainContentPane le panel qui contient la Chart Pane qui contient le graphique
	 */
	public DataTablePane(modele.ChartModel parChartModel,MainContentPane parMainContentPane){
			
		this.setPreferredSize(new Dimension(500,700));
		this.dataTable = new DataTable();
		this.dataTableModel = new modele.DataTableModel(parChartModel,parMainContentPane);
		this.dataTable.setModel(this.dataTableModel);
		this.add(new JScrollPane(dataTable,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
	}

}
