package vue;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * Panel qui permet l'enregistrement des donn�es des axes d'un graphe en barre et le nombre de groupes de celui-ci
 * 
 * @author Theo
 *
 */
public class BarChartInfoInputPanel2 extends JPanel implements ActionListener {

	/**
	 * Label d'information pour afficher "entrez le nom des axes"
	 */
	JLabel dataLabel;
	/**
	 * Label pour le type de valeur des barres du graphique
	 */
	JLabel xLabel;
	/**
	 * label pour l'axe Y
	 */
	JLabel yLabel;
	
	/**
	 * label pour le depart de l'axe Y
	 */
	JLabel startLabelY;
	/**
	 * label pour la fin de l'axe Y
	 */
	JLabel endLabelY;
	
	/**
	 * label pour le saut de valeur de l'axe Y
	 */
	JLabel tickLabelY;
	
	/**
	 * Zone de saisie pour le type de donn�es des barres du graphique
	 */
	JTextField dataTitleInputX;
	/**
	 * zone de saisie pour le titre de l'axe Y
	 */
	JTextField dataTitleInputY;
	
	
	/**
	 * zone de saisie pour la valeur de depart de l'axe Y
	 */
	JTextField startValueInputY;
	/**
	 * Zone de saisie pour la valeur de fin de l'axe Y
	 */
	JTextField endValueInputY;
	
	/**
	 * Zone de saisie pour le saut de valeur de l'axe Y
	 */
	JTextField tickValueInputY;
	
	/**
	 * Label pour le nombre de groupe du graphique en barre
	 */
	JLabel seriesNumberLabel;
	/**
	 * zone de saisie pour le nombre de groupe du graphique en barre
	 */
	JTextField seriesNumberInput;
	
	/**
	 * Bouton qui permet de passer � la saisie des informations sur les groupes (passe � la carte suivante)
	 */
	JButton nextButton;
	
	/**
	 * Le panel conteneur du ChartPane qui contient le graphique
	 */
	MainContentPane mainContentPane;
	
	/**
	 * Contrainte de type GridBagConstraints qui impose des contraintes de placement aux composants
	 */
	GridBagConstraints contrainte;

	/**
	 * La fenetre d'ajout de nouveau graphique qui contient ce panel
	 */
	InputWindow inputWindow;
	
	/**
	 * le modele du graphique en train d'�tre cr��e
	 */
	modele.ChartModel chart;
	
	/**
	 * Constructeur par defaut de la classe BarChartInfoInputPanel
	 * 
	 * @param parInputWindow La fenetre de cr�ation de nouveau graphique qui contient ce panel
	 * @param mainContentPane Le panel qui contient le ChartPane qui contient le graphique
	 * @param chartModel le modele du grahique  cr��r
	 */
	public BarChartInfoInputPanel2(InputWindow parInputWindow, MainContentPane mainContentPane, modele.ChartModel chartModel) {
		
		this.inputWindow = parInputWindow;
		this.chart = chartModel;
		
		this.mainContentPane = mainContentPane;
		this.setLayout(new GridBagLayout());
		contrainte = new GridBagConstraints();
		
		// instantiation des composants
		dataLabel = new JLabel("Entrer le nom des axes ");
		xLabel = new JLabel("Que representent les barres :");
		dataTitleInputX = new JTextField(8);
		yLabel = new JLabel("Nom de l'axe Y : ");
		dataTitleInputY = new JTextField(8);		
		startLabelY = new JLabel("Valeur de d�but de l'axe Y:");
		startValueInputY = new JTextField(4);
		endLabelY = new JLabel("Valeur de fin de l'axe Y:");
		endValueInputY = new JTextField(4);
		tickLabelY = new JLabel("Echelle de l'axe Y");
		tickValueInputY = new JTextField(4);
		seriesNumberLabel = new JLabel("Nombre de Groupes : ");
		seriesNumberInput = new JTextField(4);
		
		//ajout des composants au panel
		contrainte.gridy=1;
		contrainte.gridx=0;
		this.add(dataLabel,contrainte);
		
		contrainte.gridy=2;
		contrainte.gridx=0;
		this.add(xLabel,contrainte);
		contrainte.gridx = 4;
		this.add(this.dataTitleInputX,contrainte);
		
		contrainte.gridy=3;
		contrainte.gridx=0;
		this.add(xLabel,contrainte);
		contrainte.gridx=4;
		this.add(dataTitleInputX,contrainte);
		
		contrainte.gridy=4;
		contrainte.gridx=0;
		this.add(yLabel,contrainte);
		contrainte.gridx=4;
		this.add(dataTitleInputY,contrainte);
		
		contrainte.gridy=7;
		contrainte.gridx=0;
		this.add(startLabelY,contrainte);
		contrainte.gridx=4;
		this.add(startValueInputY,contrainte);
		
		contrainte.gridy=8;
		contrainte.gridx=0;
		this.add(endLabelY,contrainte);
		contrainte.gridx=4;
		this.add(endValueInputY,contrainte);
		
		contrainte.gridy=10;
		contrainte.gridx=0;
		this.add(tickLabelY,contrainte);
		contrainte.gridx=4;
		this.add(tickValueInputY,contrainte);
		
		contrainte.gridy = 11;
		contrainte.gridx = 0;
		this.add(seriesNumberLabel,contrainte);
		contrainte.gridx = 4;
		this.add(seriesNumberInput,contrainte);
		
		contrainte.gridy=13;
		this.add(new JLabel(" "),contrainte);
		contrainte.gridy=14;
		this.add(new JLabel(" "),contrainte);
		
		contrainte.gridy=15;
		contrainte.gridx=1;
		nextButton = new JButton("suite");
		nextButton.addActionListener(this);
		this.add(nextButton,contrainte);
	}

	
	/**
	 * 
	 */
	@Override
	public void actionPerformed(ActionEvent parEvt) {
		
		//si on change de panel (on a fini la saisie de donn�es)
		if (parEvt.getSource() == this.nextButton){
		
			//initialisation des axes et des series
			chart.yAxis = new modele.SerializableAxis(Float.parseFloat(this.startValueInputY.getText()),Float.parseFloat(this.endValueInputY.getText()),Float.parseFloat(this.tickValueInputY.getText()));
			chart.xAxis = new modele.SerializableAxis();
			
			chart.xAxis.label = this.dataTitleInputX.getText();
			chart.yAxis.label = this.dataTitleInputY.getText();
		
			chart.nbSeries = 0;
		
		
		
			chart.nbSeries = Integer.parseInt(this.seriesNumberInput.getText());
			
			//Ajout d'autant de panel de saisie pour les groupes que necessaire
			for(int i = 0; i< chart.nbSeries;i++){
			
			
				this.inputWindow.mainPanel.add(new SeriesInfoInputPanel(mainContentPane,this.inputWindow,chart,i+1),"series info panel "+(i+1));
			
			}
			//On passe a la carte suivante
			this.inputWindow.mainCardLayout.next(this.inputWindow.mainPanel);
		}
	}

}
