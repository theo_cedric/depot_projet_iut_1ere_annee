/**
 * 
 */
package vue;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
/**
 * Classe qui permet de contenir les autres panels de la fenetre principale:
 * Le panel Tableau et le panel Graphique
 * 
 * @author Theo
 *
 */
public class MainContentPane extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Le layout manager du MainContentPane
	 */
	private BorderLayout mainContentPaneLayout;
	
	/**
	 * Le panel qui contient le Graphique
	 */
	private ChartPane chartPane;
	
	/**
	 * Le panel qui contient le tableau
	 */
	private DataTablePane dataTablePane;
	
	/**
	 * Constructeur par defaut de la classe MainContentPane
	 */
	public MainContentPane(){
		
		this.mainContentPaneLayout = new BorderLayout();
		this.setLayout(mainContentPaneLayout);
		
		//Ajout des chart et table Panels
		this.chartPane = new ChartPane(ChartPane.FIRST_LANCH,new modele.ChartModel());
		this.dataTablePane = new DataTablePane(ChartPane.initModel(this.chartPane.chartModel),this);
		this.add(this.chartPane, BorderLayout.CENTER);
		this.add(this.dataTablePane,BorderLayout.EAST);
	}
	
	/**
	 * getter du chartPane
	 * 
	 * @return le chartPane
	 */
	public ChartPane getChartPane() {
		return chartPane;
	}

	/**
	 * setter du chartPane
	 * 
	 * @param chartPane le panel du graphique
	 */
	public void setChartPane(ChartPane chartPane) {
		this.chartPane = chartPane;
	}

	/**
	 * getter du tablePane
	 * 
	 * @return le tablePane
	 */
	public DataTablePane getTablePane() {
		return dataTablePane;
	}

	/**
	 * setter du tablePane
	 *
	 *@param tablePane le panneau contenant le tableau
	 */
	public void setTablePane(DataTablePane tablePane) {
		this.dataTablePane = tablePane;
	}

	/**
	 * Getter du layout Mangager de MainContentPane
	 * 
	 * @return MainContentPaneLayout (le Layout de l'objet de la classe MainContentPane)
	 */
	public BorderLayout getMainContentPaneLayout() {
		return mainContentPaneLayout;
	}

	/**
	 * Setter du layout manager de l'objejt mainContentPane
	 * 
	 * @param mainContentPaneLayout le layout du panneau
	 */
	public void setMainContentPaneLayout(BorderLayout mainContentPaneLayout) {
		this.mainContentPaneLayout = mainContentPaneLayout;
	}

}
