package outils;

import java.io.*;

/**
 * Classe contenant les methodes pour lire un fichier dans lequel est enregistr� un objet
 * et pour ecrir un objet dans un fichier
 * 
 * @author Theo
 *
 */
public class FilesMethod {

	/**
	 * Methode pour ecrire un objet dans un fichier donn� en parametre (le fichier est cr��e si il n'existe pas dans le dossier)
	 * 
	 * @param parFichier String qui permet de localiser l'emplacement ou sauvegarder le fichier
	 * @param parObject object � enregistrer
	 */
	public static void write( File parFichier, Object parObject){
		
		ObjectOutputStream flux = null;
		
		try{
			flux = new ObjectOutputStream(new FileOutputStream(parFichier));
			flux.writeObject(parObject);
			flux.flush();
			flux.close();
		}
		catch(IOException parException){
			
			System.out.println("Probleme a l'ecriture\n"+parException.toString());
		}
	}
	
	/**
	 * Methode qui permet de lire l'objet contenu dans un fichier donn� en parametre
	 * 
	 * @param parFichier Le fichier � lire
	 * @return l'objet lu dans le fichier de type Object
	 */
	public static Object read(File parFichier){
		
		ObjectInputStream flux;
		
		Object readObject = null;
		
		try{
			
			flux = new ObjectInputStream(new FileInputStream(parFichier));
			readObject = (Object)flux.readObject();
			flux.close();
		}
		catch(ClassNotFoundException parException){
			
			System.err.println(parException.toString());
			System.exit(1);
		}
		catch(IOException parException){
			
			System.err.println("Erreur lecture du fichier "+parException.toString());
			System.exit(1);
		}
		
		return readObject;
	}
}
