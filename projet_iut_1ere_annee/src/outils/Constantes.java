/**
 * 
 */
package outils;

import java.awt.Dimension;

/**
 * Classe contenant toutes les constantes du programme
 * 
 * @author Theo
 *
 */
public class Constantes {
	/**
	 * Dimension du separateur de menu utilis� dans la barre de menu
	 */
	public static final Dimension MENU_SEPARATOR_DIMENSION = new Dimension(2,100);
	
	/**
	 * la largeur maximale de l'item de menu quitter de la barre de menu
	 */
	public static final int QUIT_ITEM_MAX_WIDTH = 112;
	/**
	 * la largeur maximale de l'item de menu aide de la barre de menu
	 */
	public static final int HELP_MENU_MAX_WIDTH = 100;
	
	/**
	 * int renseignant un graphique en barre
	 */
	public static final int BAR_CHART = 1;
	/**
	 * int renseignant un graphique en bulle
	 */
	public static final int BUBBLE_CHART = 2;
	/**
	 * int renseignant un graphique en aire
	 */
	public static final int AREA_CHART = 3;
	/**
	 * int renseignant un graphique en ligne
	 */
	public static final int LINE_CHART = 4;
	/**
	 * int renseignant un graphique dispers� ( en points seulement)
	 */
	public static final int SCATTER_CHART = 5;

	/**
	 * tableau des types de graphiques (utilis� dans la JComboBox de cr�ation de graphiques)
	 */
	public static final String[] CHART_TYPE = {"Graphique d'aire","Graphique en ligne","Graphique en barre","Graphique en bulle","Graphique dispers�"};
}
